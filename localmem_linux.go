package localmem

import "os"

func getLocalMemPath() string {
	if Path != "" {
		return Path
	}

	Path = `/etc/bis/localmem`
	if _, err := os.Stat(Path); err != nil && os.IsNotExist(err) == true {
		os.MkdirAll(Path, 0666)
	}
	return Path
}
