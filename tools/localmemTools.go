package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"path/filepath"

	"bitbucket.org/Limard/localmem"
	"encoding/json"
	"strings"
)

var (
	flagKeyName = flag.String(`keyname`, "", "imported key name")
	flagCipher = flag.Bool(`cipher`, false, `cipher to save`)
)

func usage() {
	fmt.Printf("Usage: \n")
	fmt.Printf("localmemTools.exe [-cipher][-keyname=keyname] import filepath \n")
	fmt.Printf("localmemTools.exe [-json] show keyname \n")
	fmt.Printf("localmemtools.exe [-json] export keyname \n")
	fmt.Printf("localmemtools.exe delete keyname \n")
	fmt.Printf("localmemTools.exe enum \n")
	fmt.Printf("\n")

	flag.PrintDefaults()

	os.Exit(0)
}

func main() {
	flag.Usage = usage
	flag.Parse()
	key := strings.ToLower(flag.Arg(0))

	switch key {
	case "import":
		if flag.Arg(1) == "" {
			flag.Usage()
			os.Exit(1)
		}
		if err := importFile(flag.Arg(1)); err != nil {
			log.Fatalf(`%v`, err)
		}

	case "show":
		if flag.Arg(1) == "" {
			flag.Usage()
			os.Exit(1)
		}
		if err := showKey(flag.Arg(1)); err != nil {
			log.Fatalf(`%v`, err)
		}

	case "export":
		if flag.Arg(1) == "" {
			flag.Usage()
			os.Exit(1)
		}
		exportKey(flag.Arg(1))

	case "delete":
		if flag.Arg(1) == "" {
			flag.Usage()
			os.Exit(1)
		}
		deleteKey(flag.Arg(1))

	case "enum":
		enumKeys()

	default:
		flag.Usage()
	}
}

func importFile(p string) error {
	_, err := os.Stat(p)
	if err != nil {
		return err
	}

	keyName := *flagKeyName
	if keyName == ""{
		keyName = filepath.Base(p)
	}

	buf, err := ioutil.ReadFile(p)
	if err != nil {
		return err
	}

	return localmem.SetValueBuf(keyName, buf, *flagCipher)
}

func enumKeys() error {
	n := localmem.Enum()
	for _, value := range n {
		fmt.Printf("%s\r\n", value)
	}

	return nil
}

func showKey(keyName string) error {
	var v interface{}
	_, err := localmem.GetValue2(keyName, &v)
	if err != nil {
		fmt.Println("localmem error")
		return err
	}

	buf, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		fmt.Println(v)
		return err
	}

	fmt.Printf(string(buf))

	return nil
}

func exportKey(keyName string) error {
	var v interface{}
	_, err := localmem.GetValue2(keyName, &v)
	if err != nil {
		fmt.Println("localmem error")
		return err
	}

	buf, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		fmt.Println(v)
		return err
	}

	return ioutil.WriteFile(keyName, buf, 0666)
}

func deleteKey(keyName string) error {
	return localmem.Remove(keyName)
}