package localmem

import (
	"encoding/json"
	"testing"
)

func TestSetGet(t *testing.T) {
	type TESTOBJ struct {
		AA string
		BB int
	}

	obj := TESTOBJ{AA: `aaaaa`, BB: 11}

	SetValue2(`testValueP`, obj, false)
	SetValue2(`testValueC`, obj, true)

	t.Log(GetValue2(`testValueP`, &obj))
	t.Log(GetValue2(`testValueC`, &obj))
}

func TestSetGet2(t *testing.T) {
	type testOBJ struct {
		AA string // 可输出成json
		bb int    // 忽略该字段
	}

	obj := testOBJ{AA: `aaaaa`, bb: 11}

	SetValue2(`testValueP`, obj, false)
	SetValue2(`testValueC`, obj, true)

	t.Log(GetValue2(`testValueP`, &obj))
	t.Log(GetValue2(`testValueC`, &obj))
}

func TestCode(t *testing.T) {
	type TESTOBJ struct {
		AA string
	}

	obj := TESTOBJ{AA: `aaa`}

	buf, _ := json.Marshal(obj)
	t.Log(buf)
	t.Log(string(buf))

	buf = code(buf)
	t.Log(buf)
	t.Log(string(buf))

	buf, _ = json.Marshal(obj)
	t.Log(buf)
	t.Log(string(buf))

	buf = code(buf)
	t.Log(buf)
	t.Log(string(buf))
}

func TestCodeArray(t *testing.T) {
	obj := []string{`aaa`}

	buf, _ := json.Marshal(obj)
	t.Log(buf)
	t.Log(string(buf))

	buf = code(buf)
	t.Log(buf)
	t.Log(string(buf))

	buf, _ = json.Marshal(obj)
	t.Log(buf)
	t.Log(string(buf))

	buf = code(buf)
	t.Log(buf)
	t.Log(string(buf))
}
